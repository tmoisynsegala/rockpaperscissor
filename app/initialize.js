document.addEventListener('DOMContentLoaded', () => {
    //Here the game will start up
    System.loadJson("data.json", data => new Game(data))
});

/**
 * Logic stuff and behavior
 */
export class Game {
    constructor(gameInformation) {
        this.gameData = JSON.parse(gameInformation);
        this.reInit();
    }

    static _randomChoice(array) {
        return array[Math.floor(Math.random() * array.length)];
    }

    playerChoice(opt) {

        let playerWin = opt.wins.indexOf(this.computerChoice.name) !== -1;
        let playerLoses = opt.loses.indexOf(this.computerChoice.name) !== -1;
        let drawGame = opt.name === this.computerChoice.name;

        let { won, draw, lose, error, reveal } = this.gameData.messages;
        let { name } = this.computerChoice;
        let wonMsg =   opt.name + "<br>" + won + reveal + name;
        let drawMsg =  opt.name + "<br>" + draw + reveal + name;
        let loseMsg =  opt.name + "<br>" + lose + reveal + name;
        let errorMsg = opt.name + "<br>" + error + reveal + name;

        if (playerWin)
            Screen.showMessage(wonMsg);
        else if (drawGame)
            Screen.showMessage(drawMsg);
        else if (playerLoses)
            Screen.showMessage(loseMsg);
        else //error, probably a type missing properties, or conflicting
            Screen.showMessage(errorMsg);

        Game._invalidateInput();

        let { timeForReboot } = this.gameData.configs;
        setTimeout(() => this.reInit(), timeForReboot);
    }

    reInit() {
        Screen.showMessage(this.gameData.messages.welcome);
        this.computerChoice = Game._randomChoice(this.gameData.options);
        this.buildUI(this.gameData.options);
        if( this.playingAlone )
            this.timerPlayingAlone = setTimeout( () => {
                this.playerChoice( Game._randomChoice( this.gameData.options ) )
            }, this.gameData.configs.timeForPlayAlone);
    }

    buildUI(options) {

        options.forEach(
            opt => {
                let button = Screen.createButton();
                button.innerHTML = opt.name;
                button.addEventListener("click", () => this.playerChoice(opt));
            }
        );

        //computer play button
        let button = Screen.createButton();
        if( !this.playingAlone )
            button.innerHTML = this.gameData.messages.playAlone;
        else
            button.innerHTML = this.gameData.messages.stopPlayAlone;
        button.style = "position: absolute; top: 0px; left: 0px;";
        button.addEventListener("click", () => this.playAlone());

        Screen.showMessage(this.gameData.messages.welcome);
    }

    playAlone () {
        Game._invalidateInput();
        Screen.showMessage(this.gameData.messages.welcome);
        if( !this.playingAlone ) {
            this.playingAlone = true;
        }
        else
        {
            clearTimeout( this.timerPlayingAlone );
            this.playingAlone = false;
        }
        this.reInit();
    }

    static _invalidateInput() {

        //not the best option, I know, but gets the job done, for simplicity sake
        while (document.getElementsByTagName("button")[0]) {
            let button = document.getElementsByTagName("button")[0];
            button.parentNode.removeChild(button);
        }
    }
}


/**
 * Easy to access screen/page objects
 */
export class Screen {
    static getRPS() {
        return document.getElementById("rps");
    }

    static getMessageId() {
        return document.getElementById("message");
    }

    static createButton() {
        let button = document.createElement("button");
        Screen.getRPS().appendChild(button);
        return button;
    }

    static showMessage(message) {
        Screen.getMessageId().innerHTML = message;
    }
}


/**
 * To deal with system, browser, load stuff and etc
 */
export class System {

    static loadJson(filepath, callback) {
        const request = new XMLHttpRequest();
        request.overrideMimeType("application/json");
        request.open('GET', filepath, true);
        request.onreadystatechange = function () {
            if (request.readyState == 4 && request.status == "200") {
                callback(request.responseText);
            }
        };
        request.send(null);
    }
}